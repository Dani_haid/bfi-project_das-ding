<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <title>Privacy Policy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- Navigation -->
    <?php require "inc/nav.inc.php"; ?>



    <main>
        <!-- Hero Element Detail Site -->
        <div class="subhero">
            <div class="subhero-image-wrapper">
                <img src="https://images.pexels.com/photos/408503/pexels-photo-408503.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
            </div>
        </div>

        <!-- Text Block -->
        <div class="custom-container-8 mx-auto mt-8">
            <div>
                <h1>Privacy Policy.</h1>
                <div class="py-4">
                    <h2>Datenschutz</h2>
                    <p>Wir haben diese Datenschutzerklärung (Fassung 01.01.1970-121604293) verfasst, um Ihnen gemäß der
                        Vorgaben der <a href="https://eur-lex.europa.eu/legal-content/DE/ALL/?uri=celex%3A32016R0679&tid=121604293">Datenschutz-Grundverordnung
                            (EU) 2016/679</a> zu erklären, welche Informationen wir sammeln, wie wir Daten verwenden und
                        welche Entscheidungsmöglichkeiten Sie als Besucher dieser Webseite haben.
                        Leider liegt es in der Natur der Sache, dass diese Erklärungen sehr technisch klingen, wir haben
                        uns bei der Erstellung jedoch bemüht die wichtigsten Dinge so einfach und klar wie möglich zu
                        beschreiben.</p>
                    <h2>Automatische Datenspeicherung</h2>
                    <p>
                        Wenn Sie heutzutage Webseiten besuchen, werden gewisse Informationen automatisch erstellt und
                        gespeichert, so auch auf dieser Webseite.
                        Wenn Sie unsere Webseite so wie jetzt gerade besuchen, speichert unser Webserver (Computer auf
                        dem diese Webseite gespeichert ist) automatisch Daten wie
                        <ul>
                            <li>die Adresse (URL) der aufgerufenen Webseite</li>
                            <li> Browser und Browserversion</li>
                            <li>das verwendete Betriebssystem</li>
                            <li>die Adresse (URL) der zuvor besuchten Seite (Referrer URL)</li>
                            <li>den Hostname und die IP-Adresse des Geräts von welchem aus zugegriffen wird</li>
                            <li>Datum und Uhrzeit</li>
                        </ul>
                        in Dateien (Webserver-Logfiles).</p>
                    <p>In der Regel werden Webserver-Logfiles zwei Wochen gespeichert und danach automatisch gelöscht.
                        Wir geben diese Daten nicht weiter, können jedoch nicht ausschließen, dass diese Daten beim
                        Vorliegen von rechtswidrigem Verhalten eingesehen werden.</p>
                    <h2>Cookies</h2>
                    <p>Unsere Webseite verwendet HTTP-Cookies, um nutzerspezifische Daten zu speichern.Im Folgenden
                        erklären wir, was Cookies sind und warum Sie genutzt werden, damit Sie die folgende
                        Datenschutzerklärung besser verstehen.</p>
                    <h2>Wie kann ich Cookies löschen?</h2>
                    <p>Wie und ob Sie Cookies verwenden wollen, entscheiden Sie selbst. Unabhängig von welchem Service
                        oder welcher Webseite die Cookies stammen, haben Sie immer die Möglichkeit Cookies zu löschen,
                        zu deaktivieren oder nur teilweise zuzulassen. Zum Beispiel können Sie Cookies von
                        Drittanbietern blockieren, aber alle anderen Cookies zulassen.</p>
                    <p>Wenn Sie feststellen möchten, welche Cookies in Ihrem Browser gespeichert wurden, wenn Sie
                        Cookie-Einstellungen ändern oder löschen wollen, können Sie dies in Ihren Browser-Einstellungen
                        finden</p>
                    <h2>Speicherung persönlicher Daten</h2>
                    <p>Persönliche Daten, die Sie uns auf dieser Website elektronisch übermitteln, wie zum Beispiel
                        Name, E-Mail-Adresse, Adresse oder andere persönlichen Angaben im Rahmen der Übermittlung eines
                        Formulars oder Kommentaren im Blog, werden von uns gemeinsam mit dem Zeitpunkt und der
                        IP-Adresse nur zum jeweils angegebenen Zweck verwendet, sicher verwahrt und nicht an Dritte
                        weitergegeben.</p>
                    <p>Wir nutzen Ihre persönlichen Daten somit nur für die Kommunikation mit jenen Besuchern, die
                        Kontakt ausdrücklich wünschen und für die Abwicklung der auf dieser Webseite angebotenen
                        Dienstleistungen und Produkte. Wir geben Ihre persönlichen Daten ohne Zustimmung nicht weiter,
                        können jedoch nicht ausschließen, dass diese Daten beim Vorliegen von rechtswidrigem Verhalten
                        eingesehen werden.</p>
                    <p>Wenn Sie uns persönliche Daten per E-Mail schicken – somit abseits dieser Webseite – können wir
                        keine sichere Übertragung und den Schutz Ihrer Daten garantieren. Wir empfehlen Ihnen,
                        vertrauliche Daten niemals unverschlüsselt per E-Mail zu übermitteln.</p>
                    <h2>Google Fonts Datenschutzerklärung</h2>
                    <p>Auf unserer Website verwenden wir Google Fonts. Das sind die “Google-Schriften” der Firma Google
                        Inc. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow
                        Street Dublin 4, Irland) für alle Google-Dienste verantwortlich.</p>
                    <p>Für die Verwendung von Google-Schriftarten müssen Sie sich nicht anmelden bzw. ein Passwort
                        hinterlegen. Weiters werden auch keine Cookies in Ihrem Browser gespeichert. Die Dateien (CSS,
                        Schriftarten/Fonts) werden über die Google-Domains fonts.googleapis.com und fonts.gstatic.com
                        angefordert. Laut Google sind die Anfragen nach CSS und Schriften vollkommen getrennt von allen
                        anderen Google-Diensten. Wenn Sie ein Google-Konto haben, brauchen Sie keine Sorge haben, dass
                        Ihre Google-Kontodaten, während der Verwendung von Google Fonts, an Google übermittelt werden.
                        Google erfasst die Nutzung von CSS (Cascading Style Sheets) und der verwendeten Schriftarten und
                        speichert diese Daten sicher. Wie die Datenspeicherung genau aussieht, werden wir uns noch im
                        Detail ansehen.</p>
                    <h2>Newsletter Datenschutzerklärung</h2>
                    <p>Wenn Sie sich für unseren Newsletter eintragen übermitteln Sie die oben genannten persönlichen
                        Daten und geben uns das Recht Sie per E-Mail zu kontaktieren. Die im Rahmen der Anmeldung zum
                        Newsletter gespeicherten Daten nutzen wir ausschließlich für unseren Newsletter und geben diese
                        nicht weiter. </p>
                    <p>Sollten Sie sich vom Newsletter abmelden – Sie finden den Link dafür in jedem Newsletter ganz
                        unten – dann löschen wir alle Daten die mit der Anmeldung zum Newsletter gespeichert wurden.
                    </p>
                </div>
            </div>
        </div>
    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php"; ?>

</body>
<script src="js/main.js"></script>

</html>