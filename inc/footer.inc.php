    <footer>
        <div class="footer-bar d-flex flex-column flex-sm-row justify-content-between py-5 py-sm-3 px-sm-5">
            <div class="menu-bottom footer-item-wrapper d-flex">
                <div class="footer-item">
                    <a href="impressum.php" class="nav-item nav-footer <?php active('impressum.php'); ?>">Impressum</a>
                </div>
                <div class="footer-item">
                    <a href="Privacy.php" class="nav-item nav-footer <?php active('Privacy.php'); ?>">Privacy Policy</a>
                </div>
            </div>
            <div class="footer-social">
                <a href="#" class="fa fa-facebook"></a>
                <a href="#" class="fa fa-linkedin"></a>
                <a href="#" class="fa fa-instagram"></a>
            </div>
            <div class="footer-logo mt-3 mt-sm-0">
                <img src="logo_mybicycle_white.svg" alt="logo_mybicycle_white">
            </div>
        </div>
    </footer>