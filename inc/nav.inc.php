<header class="text-white shadow">
    <div id="navigation" class="navigation d-flex justify-content-between align-items-center">
        <div class="nav-logo"><a href="index.php">
                <img src="logo_mybicycle.svg" alt="logo_mybicycle"></a>
        </div>

        <div id="burger-menu" class="burger">
            <span class="bar bar1"></span>
            <span class="bar bar2"></span>
            <span class="bar bar4"></span>
        </div>

        <nav class="menu">
            <?php
            function active($current_page)
            {
                $url_array =  explode('/', $_SERVER['REQUEST_URI']);
                $url = end($url_array);
                if ($current_page == $url) {
                    echo 'active';
                }
            }
            ?>
            <ul id="menu" class="nav-list d-flex flex-column flex-lg-row justify-content-center m-0">
                <li><a class="nav-item nav-header <?php active('geometry.php'); ?>" href="geometry.php">Geometry</a></li>
                <li><a class="nav-item nav-header <?php active('optics.php'); ?>" href="optics.php">Optics</a></li>
                <li><a class="nav-item nav-header <?php active('components.php'); ?>" href="components.php">Components</a></li>
                <li><a class="nav-item nav-header <?php active('history.php'); ?>" href="history.php">History</a></li>
                <li><a class="nav-item nav-header <?php active('images.php'); ?>" href="images.php">Images</a></li>
            </ul>
            <div id="close-menu" class="close-btn">
                <span class="bar x1"></span>
                <span class="bar x2"></span>
            </div>
        </nav>
    </div>
</header>