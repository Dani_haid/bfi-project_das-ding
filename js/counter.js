let countUpDate = new Date("Jan 5, 2017 15:37:25").getTime();
let x = setInterval(function() {
let now = new Date().getTime();

let distance = now - countUpDate;

// Time calculations for days, hours, minutes and seconds
let days = Math.floor(distance / (1000 * 60 * 60 * 24));
let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
let seconds = Math.floor((distance % (1000 * 60)) / 1000);

// Output the result in an element with id="counter"
document.getElementById("counter").innerHTML = days + "d " + hours + "h "
+ minutes + "m " + seconds + "s ";

}, 1000);