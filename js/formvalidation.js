function validateForm(){
    let email = document.getElementById("nl-email");
    let fname = document.getElementById("nl-firstname");
    let lname = document.getElementById("nl-lastname");
    let prof = document.getElementById("nl-profession");
    let gdpo = document.getElementById("nl-gdpo");
    let error = document.getElementById("errorMsg");
    let msg = "";

    document.querySelector("form").addEventListener("invalid",
    function (bError){
        bError.preventDefault();
    },true);

    if(!email.checkValidity()){
        msg = "Please enter your email adress!";
    }

    if(!fname.checkValidity()){
        msg = "Please enter your first name!";
    }

    if(!lname.checkValidity()){
        msg = "Please enter your last name!";
    }

    if(!gdpo.checkValidity()){
        msg = "Please accept the gdpo regulations!";
    }
    error.innerHTML = msg;

    return true;
}