<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <title>Optics</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- Navigation -->
    <?php require "inc/nav.inc.php"; ?>

    <main>
        <!-- Hero Element Detail Site -->
        <div class="subhero">
            <div class="subhero-image-wrapper">
                <img src="https://images.pexels.com/photos/2179483/pexels-photo-2179483.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
            </div>
        </div>

        <!-- Text Block -->
        <div class="custom-container-8 mx-auto mt-8">
            <div>
                <h1 class="mb-4">optics.</h1>
                <h2>Colors</h2>
                <div class="pb-4 pt-3">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat. Lorem ipsum dolor sit amet consectetur
                        adipisicing
                        elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat.
                    </p>
                </div>

                <div class="color-fields">
                    <div class="row row-cols-auto">
                        <div class="col mb-3">
                            <div class="color1"></div>
                            <div>Ninja black</div>
                        </div>
                        <div class="col mb-3">
                            <div class="color2"></div>
                            <div>Ocean blue</div>
                        </div>
                        <div class="col mb-3">
                            <div class="color3"></div>
                            <div>Sunflower yellow</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="damage-section">
                <div>
                    <h2>Damages</h2>
                </div>
                <div class="image-row mb-3">
                    <div class="row">
                        <div class="img col-md-4">
                            <img src="https://images.pexels.com/photos/6709/vintage-grey-airplane-plane.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
                        </div>
                        <div class="img col-md-4">
                            <img src="https://images.pexels.com/photos/4874228/pexels-photo-4874228.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" alt="">
                        </div>
                        <div class="img col-md-4">
                            <img src="https://images.pexels.com/photos/78793/automotive-defect-broken-car-wreck-78793.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php"; ?>

</body>
<script src="js/main.js"></script>

</html>