<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <title>Impressum</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- Navigation -->
    <?php require "inc/nav.inc.php"; ?>

    <main>
        <!-- Hero Element Detail Site -->
        <div class="subhero">
            <div class="subhero-image-wrapper">
                <img src="https://images.pexels.com/photos/408503/pexels-photo-408503.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
            </div>
        </div>

        <!-- Text Block -->
        <div class="custom-container-8 mx-auto mt-8">
            <div>
                <h1>Impressum.</h1>
                <div class="py-4">
                    <p>Informationspflicht laut §5 E-Commerce Gesetz, <br />
                        §14 Unternehmensgesetzbuch, <br />
                        §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz.</p>

                    <p>Daniela Stöhr</br>
                        Innsbruckerstrasse 42a<br />
                        6176 Völs<br />
                        Austria</p>

                    <h2>EU-Streitschlichtung</h2>
                    <p>Gemäß Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-Verordnung) möchten wir Sie über die Online-Streitbeilegungsplattform (OS-Plattform) informieren.Verbraucher haben die Möglichkeit, Beschwerden an die Online Streitbeilegungsplattform der Europäischen Kommission unter http://ec.europa.eu/odr?tid=121604293 zu richten. Die dafür notwendigen Kontaktdaten finden Sie oberhalb in unserem Impressum.
                        Wir möchten Sie jedoch darauf hinweisen, dass wir nicht bereit oder verpflichtet sind, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.</p>

                    <h2>Haftung für Inhalte dieser Webseite</h2>
                    <p>
                        Wir entwickeln die Inhalte dieser Webseite ständig weiter und bemühen uns korrekte und aktuelle Informationen bereitzustellen. Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Webseite übernehmen, speziell für jene die seitens Dritter bereitgestellt wurden.
                        Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitten wir Sie uns umgehend zu kontaktieren, Sie finden die Kontaktdaten im Impressum.
                    </p>
                    <h2>Haftung für Links auf dieser Webseite</h2>
                    <p>
                        Unsere Webseite enthält Links zu anderen Webseiten für deren Inhalt wir nicht verantwortlich sind. Haftung für verlinkte Websites besteht laut § 17 ECG für uns nicht, da wir keine Kenntnis rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und wir Links sofort entfernen würden, wenn uns Rechtswidrigkeiten bekannt werden.
                        Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitten wir Sie uns zu kontaktieren, Sie finden die Kontaktdaten im Impressum.
                    </p>
                    <h2>Bildernachweis</h2>
                    <p>
                        Die Bilder, Fotos und Grafiken auf dieser Webseite sind urheberrechtlich geschützt. Sollten Sie auf dieser Webseite Inhalte finden, die das Urheberrecht verletzen, bitten wir Sie uns zu kontaktieren.
                    </p>
                </div>
            </div>
        </div>
    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php"; ?>

</body>
<script src="js/main.js"></script>

</html>