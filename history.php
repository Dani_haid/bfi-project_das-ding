<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <title>History</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- Navigation -->
    <?php require "inc/nav.inc.php";?>
 
    <main>
        <!-- Hero Element Detail Site -->
        <div class="subhero">
            <div class="subhero-image-wrapper">
                <img src="https://images.pexels.com/photos/408503/pexels-photo-408503.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                    alt="">
            </div>
        </div>

        <!-- Text Block -->
        <div class="custom-container-8 mx-auto mt-8">
            <div>
                <h1>history.</h1>
                <div class="py-4">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat. Lorem ipsum dolor sit amet consectetur
                        adipisicing
                        elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat.
                    </p>
                </div>
            </div>
        </div>

        <div class="fact-wrapper mb-10 mt-4">
            <div class="row">
                <div class="fact d-flex flex-column align-items-center mb-5">
                    <div id="counter" class="counter"></div>
                    <span class="fact-title">Age</span>
                </div>
                <div class="fact-items d-sm-flex flex-sm-row flex-sm-wrap">
                    <div class="fact b-grey d-flex flex-column align-items-center p-4 col-sm-6 order-sm-1">
                        <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                        <span class="light"> 01.01.2017</span>
                        <span class="fact-title light"> Date of Birth</span>
                    </div>

                    <div class="fact d-flex flex-column align-items-center p-4 col-sm-6 order-sm-2">
                        <i class="fa fa-bicycle" aria-hidden="true"></i>
                        <span>220000 km</span>
                        <span class="fact-title"> Kilometers</span>
                    </div>

                    <div class="fact b-grey d-flex flex-column align-items-center p-4 col-sm-6 order-sm-4">
                        <i class="fa fa-ban" aria-hidden="true"></i>
                        <span class="light">5</span>
                        <span class="fact-title light"> Crashes</span>
                    </div>

                    <div class="fact d-flex flex-column align-items-center p-4 col-sm-6 order-sm-3">
                        <i class="fa fa-bug" aria-hidden="true"></i>
                        <span>33</span>
                        <span class="fact-title"> Flat tyres</span>
                    </div>
                </div>
            </div>
        </div>

<!--         <div class="custom-container-8 mx-auto">
            <h2>Places</h2>
            <div>map</div>
        </div> -->

    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php";?>

</body>
<script src="js/main.js"></script>
<script src="js/counter.js"></script>

</html>