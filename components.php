<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <title>Components</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<!-- Navigation -->
<?php require "inc/nav.inc.php";?>  

    <main>
        <!-- Hero Element Detail Site -->
        <div class="subhero">
            <div class="subhero-image-wrapper">
                <img src="https://images.pexels.com/photos/162631/blacksmith-tools-shop-rustic-162631.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                    alt="">
            </div>
        </div>

        <!-- Text Block -->
        <div class="custom-container-8 mx-auto mt-8">
            <div>
                <h1 class="mb-4">Components.</h1>
                <div class="pb-4 pt-3 pb-md-5">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat. Lorem ipsum dolor sit amet consectetur
                        adipisicing
                        elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat.
                    </p>
                </div>
            </div>

            <div class="component-wrapper mb-5">
                <div class="row">
                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                    <div class="component mb-5 col-md-4">
                        <div class="component-inner">
                            <div class="component-img">
                                <img src="https://images.pexels.com/photos/209239/pexels-photo-209239.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                                    alt="">
                                <div class="component-layer"></div>
                            </div>
                            <div class="component-text">
                                <p>This is a bike pedal. You need it for pedaling to move foreward. It's an essential
                                    item on the bike.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php";?>

</body>
<script src="js/main.js"></script>

</html>