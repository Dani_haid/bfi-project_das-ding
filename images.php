<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <!-- UniteGallery -->
    <script type='text/javascript' src='unitegallery/dist/js/unitegallery.min.js'></script> 
		<link rel='stylesheet' href='unitegallery/dist/css/unite-gallery.css' type='text/css' /> 
		<script type='text/javascript' src='unitegallery/dist/themes/tiles/ug-theme-tiles.js'></script> 

    <title>Images</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- Navigation -->
    <?php require "inc/nav.inc.php"; ?>

    <main>
        <!-- Title -->
        <div class="custom-container-8 mx-auto mt-8">
            <div>
                <h1>images.</h1>
            </div>
        </div>

        <!-- image gallery -->

        <div id="gallery" style="display:none;">

            <img alt="Image 1 Title" src="https://images.pexels.com/photos/247929/pexels-photo-247929.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-image="https://images.pexels.com/photos/247929/pexels-photo-247929.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-description="Image 1 Description">

            <img alt="Image 2 Title" src="https://images.pexels.com/photos/2179483/pexels-photo-2179483.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=500&w=360" data-image="https://images.pexels.com/photos/2179483/pexels-photo-2179483.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=500&w=360" data-description="Image 2 Description">

            <img alt="Image 3 Title" src="https://images.pexels.com/photos/4874228/pexels-photo-4874228.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" data-image="https://images.pexels.com/photos/4874228/pexels-photo-4874228.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" data-description="Image 2 Description">

            <img alt="Image 1 Title" src="https://images.pexels.com/photos/247929/pexels-photo-247929.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-image="https://images.pexels.com/photos/247929/pexels-photo-247929.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-description="Image 1 Description">


            <img alt="Image 4 Title" src="https://images.pexels.com/photos/4819642/pexels-photo-4819642.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-image="https://images.pexels.com/photos/4819642/pexels-photo-4819642.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-description="Image 4 Description">

            <img alt="Image 2 Title" src="https://images.pexels.com/photos/2179483/pexels-photo-2179483.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=500&w=360" data-image="https://images.pexels.com/photos/2179483/pexels-photo-2179483.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=500&w=360" data-description="Image 2 Description">

            <img alt="Image 3 Title" src="https://images.pexels.com/photos/4874228/pexels-photo-4874228.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" data-image="https://images.pexels.com/photos/4874228/pexels-photo-4874228.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" data-description="Image 3 Description">



            <img alt="Image 3 Title" src="https://images.pexels.com/photos/4874228/pexels-photo-4874228.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" data-image="https://images.pexels.com/photos/4874228/pexels-photo-4874228.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" data-description="Image 3 Description">

        </div>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery("#gallery").unitegallery({
                    gallery_theme: "tiles",
                    tiles_type: "nested",
                    tile_enable_image_effect: true,
                    tile_enable_overlay: false,
                    tile_enable_icons: false,
                    lightbox_show_numbers: false,
                });

            });
        </script>



    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php"; ?>

</body>
<script src="js/main.js"></script>

</html>