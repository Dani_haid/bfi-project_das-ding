<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <title>My Bicycle</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- Navigation -->
    <?php require "inc/nav.inc.php"; ?>

    <main>
        <!-- Hero Element -->
        <div class="hero container-fluid position-relative">
            <h1 class="text-uppercase">My Bicycle.</h1>
            <div class="mb-3 text-typing">
                <div class="animate-typing" data-animate-loop="true" data-type-speed="250" data-type-delay="200" data-remove-speed="100" data-remove-delay="500" data-cursor-speed="700">
                    is awesome!|
                    is great!|
                    is the best!
                </div>
            </div>



            <div class="hero-image-wrapper">
                <div class="first-hero-img">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
                </div>
                <div class="second-hero-img">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
                </div>
            </div>
        </div>
        <!-- Blackbox Element -->
        <div class="blackbox b-dark container-fluid">
            <div class="custom-container-8 mx-auto py-10">
                <div class="row flex-column-reverse flex-md-row">
                    <div class="box-col1 col-md-8">
                        <div class="mb-5 pt-1">
                            <p>
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                                Cum voluptate nesciunt sint temporibus inventore,
                                id molestias quasi ab corporis odio minima dolorem officia sequi dignissimos totam
                                magni?
                                Obcaecati, dicta pariatur.
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                                Cum voluptate nesciunt sint temporibus inventore,
                                id molestias quasi ab corporis odio minima dolorem officia sequi dignissimos totam
                                magni?
                                Obcaecati, dicta pariatur.
                            </p>
                        </div>
                        <a href="geometry.php" class="first-btn">Learn More</a>
                    </div>
                    <div class="box-col2 col-md-4 mb-5 mb-md-0">
                        <div>
                            <img src="https://images.pexels.com/photos/963278/pexels-photo-963278.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Two Column Element -->
        <div class="container-fluid mt-10 mb-14">
            <div class="column-wrapper">
                <div class="row">
                    <div class="column1 col-md-6 mb-5 mb-md-0">
                        <div class="column-img">
                            <img src="https://images.pexels.com/photos/2179483/pexels-photo-2179483.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
                        </div>
                        <div>
                            <div class="column-text">
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                                    Cum voluptate nesciunt sint temporibus inventore,</p>
                            </div>
                            <a href="optics.php" class="first-btn">Learn More</a>
                        </div>
                    </div>
                    <div class="column2 col-md-6">
                        <div class="column-img">
                            <img src="https://images.pexels.com/photos/408503/pexels-photo-408503.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
                        </div>
                        <div>
                            <div class="column-text">
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                                    Cum voluptate nesciunt sint temporibus inventore,</p>
                            </div>
                            <a href="history.php" class="first-btn">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Newsletter registration Form -->

        <div>
            <div class="newsletter custom-container-8 mx-auto mb-10 p-5 p-md-10">
                <h2 class="pb-4">Register for the MYBICYLCE Newsletter</h2>
                <form id="nl-form" name="nl-form" method="post" action="php/nl_form.php">
                    <div class="row">
                        <div class="mb-3 col-lg-6">
                            <label for="input-email">Email Adress*</label>
                            <input name="nl-email" type="email" id="nl-email" class="form-control" required>
                        </div>
                        <div class="mb-3 col-lg-6">
                            <label for="input-firstname">Firstname*</label>
                            <input name="nl-firstname" type="text" id="nl-firstname" class="form-control" required>
                        </div>
                        <div class="mb-3 col-lg-6">
                            <label for="input-lastname">Lastname*</label>
                            <input name="nl-lastname" type="text" id="nl-lastname" class="form-control" required>
                        </div>
                        <div class="mb-3 col-lg-6">
                            <label for="input-profession">Profession</label>
                            <input name="nl-profession" type="text" id="nl-profession" class="form-control">
                        </div>
                    </div>
                    <div class="mb-3 form-check">
                        <input name="nl-gdpo" type="checkbox" id="nl-gdpo" class="form-check-input" required>
                        <label for="nl-gdpo" class="form-check-label">I would like to register and I'm accepting
                            the privacy policy</label>
                    </div>
                    <input type="hidden" name="aktion" value="speichern">
                    <button onclick="validateForm()" type="submit" value="submit" class="btn first-btn">Register now</button>
                    <div id="errorMsg"></div>
                </form>
            </div>
        </div>

    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php"; ?>

</body>
<script src="js/main.js"></script>
<script src="js/Text_typing/jquery.animateTyping.js"></script>
<script src="js/formvalidation.js"></script>

</html>