<?php
require "db.php";

$email = "";
$firstname = "";
$lastname = "";
$profession = "";
$gdpo = "";
$aktion = "";
$registrationdate = date("Y-m-d");

if (isset($_POST["nl-email"])){
    $email = $_POST["nl-email"];    
}

if (isset($_POST["nl-firstname"])){
    $firstname = $_POST["nl-firstname"];    
}

if (isset($_POST["nl-lastname"])){
    $lastname = $_POST["nl-lastname"];    
}

if (isset($_POST["nl-profession"])){
    $profession = $_POST["nl-profession"];    
}

if (isset($_POST["nl-gdpo"])){
    $gdpo = $_POST["nl-gdpo"];    
}

$insert = $connection->prepare(
    "INSERT INTO contacts(email, firstname, lastname, profession, gdpo, registrationdate) VALUES(?,?,?,?,?,?)"
);

$insert->bind_param("ssssss", $email, $firstname, $lastname, $profession, $gdpo, $registrationdate);
if($insert->execute()){
    header("Location:/Das_Ding/nl_thankyou.php");
    die();
}

?>