<?php

require "php/db.php";

//SQL
$sql = 'SELECT * FROM Geometry';

//Abfrage ausführen
$result = $connection->query($sql);

if ($result->num_rows) {
    while ($row = $result->fetch_object()) {
        $data[] = $row;
    }
?>

            <div class="table-wrapper col-md-6">
                <Table id="bike-geometry">
                    <thead>
                        <tr>
                            <th>Part Key</th>
                            <th>Part Name</th>
                            <th>Size (in cm)</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        foreach ($data as $content) {
                        ?>
                            <tr>
                                <td><?php echo $content->partdescription; ?></td>
                                <td><?php echo $content->partname; ?></td>
                                <td><?php echo $content->partsize; ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </Table>
            <?php
        }
            ?>
            </div>