<!DOCTYPE html>
<html lang="de">

<head>
    <?php require "inc/head.inc.php";?> 
    <title>Geometry</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

    <!-- Navigation -->
    <?php require "inc/nav.inc.php"; ?>

    <main>
        <!-- Hero Element Detail Site -->
        <div class="subhero">
            <div class="subhero-image-wrapper">
                <img src="https://images.pexels.com/photos/963278/pexels-photo-963278.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
            </div>
        </div>

        <!-- Text Block -->
        <div class="custom-container-8 mx-auto mt-8">
            <div>
                <h1>Geometry.</h1>
                <div class="py-4">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat. Lorem ipsum dolor sit amet consectetur
                        adipisicing
                        elit.
                        Quibusdam odit fugit ducimus, ut qui itaque ea possimus.
                        Ullam ducimus suscipit facilis, aspernatur nobis dolorem
                        cum amet quos perferendis reprehenderit! Quaerat.
                    </p>
                </div>
            </div>

            <div class="table-group mt-3">
                <div class="row flex-md-row-reverse">
                    <div class="table-img col-md-6">
                        <img src="https://images.pexels.com/photos/545004/pexels-photo-545004.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="">
                    </div>

                    <? include('php/db_geometry.php'); ?>

                </div>
            </div>

        </div>


    </main>


    <!-- Footer -->
    <?php require "inc/footer.inc.php"; ?>

</body>
<script src="js/main.js"></script>

</html>